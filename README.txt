file structure:
FIT2097_A1(working project folder)
README.txt(copy of this read me text file)
class diagram.jpeg(class diagram of the project)

Imported meshes from external websites:
https://opengameart.org/content/office-desk-and-chair-set
https://opengameart.org/content/switch
https://opengameart.org/content/small-hospital-bed
https://opengameart.org/content/prison-cell

sound found from the internet:

iron door sound effect
https://www.zapsplat.com/music/swinging-iron-door-squeak-1/

wooden door sound effect
https://www.zapsplat.com/music/door-opening-wooden-internal-door-opened-1/

Electricity arcing
https://www.zapsplat.com/music/electricity-arcing-sound-design/

all resource above are made by zapsplat.

Light Switch(edited for adapting switch effect)
https://www.zapsplat.com/music/light-switch-single-double-household/
by PMSFX

Custom meshes/blueprints' location in the project: 
content/levels/props and content/blueprints

Name of the main level: content/levels/LV1b

Customized starter content materials/objects:
	- M_Glass
	- m_metal_brushed_nickel
	- m_metal_chrome
	- ThirdPersonCharacterPlayer

link of the drive: https://drive.google.com/drive/folders/1cz5zbMSMQ30EnRCy9A-2lqkS4sKTIFtz?usp=sharing