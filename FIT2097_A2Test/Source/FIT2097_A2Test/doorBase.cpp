// Fill out your copyright notice in the Description page of Project Settings.

#include "doorBase.h"


// Sets default values
AdoorBase::AdoorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	doorFrame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door frame"));
	RootComponent = doorFrame;

	door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door"));
	door->SetupAttachment(RootComponent);

	isPointedTo = false;
	isLocked = false;
	isGlow = false;
	isOpened = false;
	doorName = "door";

	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void AdoorBase::BeginPlay()
{
	Super::BeginPlay();

	glowDoor();

}

// Called every frame
void AdoorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AdoorBase::getIsPointedTo()
{
	return isPointedTo;
}

bool AdoorBase::getIsLocked()
{
	return isLocked;
}

bool AdoorBase::getIsOpened()
{
	return isOpened;
}

void AdoorBase::setIsPointedTo(bool inBool)
{
	isPointedTo = inBool;
}

void AdoorBase::setIsLocked(bool inBool)
{
	isLocked = inBool;
}

void AdoorBase::setIsOpened(bool inBool)
{
	isOpened = inBool;
}

FString AdoorBase::getDoorName()
{
	return doorName;
}

bool AdoorBase::getIsGlow()
{
	return isGlow;
}

void AdoorBase::setIsGlow(bool inBool)
{
	isGlow = inBool;
}

