// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyGameMode2.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A2TEST_API AMyGameMode2 : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	void createHUD();
	void startGame();
	void countDown();
	void gameOver();
	void gameClear();

protected:
	// Called when the game starts or when spawned
	bool isGameClear;
	bool isGameOver;

	//UPROPERTY()
	//UUserWidget* gamewidget;






	
	
};
