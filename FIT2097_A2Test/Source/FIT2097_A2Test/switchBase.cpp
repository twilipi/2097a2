// Fill out your copyright notice in the Description page of Project Settings.

#include "switchBase.h"


// Sets default values
AswitchBase::AswitchBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	switchMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("switch"));
	RootComponent = switchMesh;

	isPointedTo = false;
	isEnabled = false;
	isGlow = false;
	switchID = 0;
	switchName = "switch";
	
	bReplicates = true;
	bReplicateMovement = true;

}

// Called when the game starts or when spawned
void AswitchBase::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void AswitchBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AswitchBase::getIsPointedTo()
{
	return isPointedTo;
}

bool AswitchBase::getIsEnabled()
{
	return isEnabled;
}

void AswitchBase::setIsPointedTo(bool inBool)
{
	isPointedTo = inBool;
}

void AswitchBase::setIsEnabled(bool inBool)
{
	isEnabled = inBool;
	//do something when enabled like changeSwitchStatus()???
}

bool AswitchBase::getIsGlow()
{
	return isGlow;
}

void AswitchBase::setIsGlow(bool inBool)
{
	isGlow = inBool;
}

FString AswitchBase::getSwitchName()
{
	return switchName;
}

int32 AswitchBase::getID()
{
	return switchID;
}

