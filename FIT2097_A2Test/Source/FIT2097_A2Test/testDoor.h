// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "testDoor.generated.h"

UCLASS()
class FIT2097_A2TEST_API AtestDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AtestDoor();

	//meshes
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* doorFrame;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* door;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = "door control")
		bool isPointedTo;
	UPROPERTY(EditAnywhere, Category = "door control")
		bool isLocked;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable, Category = "door control")
		bool getIsPointedTo();

	UFUNCTION(BlueprintCallable, Category = "door control")
		bool getIsLocked();

	UFUNCTION(BlueprintCallable, Category="door control")
		void setIsPointedTo(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "door control")
		void setIsLocked(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "door control")
		FString GetPickupName();

	UFUNCTION(BlueprintImplementableEvent, Category = "door control")
		void openDoor();

};
