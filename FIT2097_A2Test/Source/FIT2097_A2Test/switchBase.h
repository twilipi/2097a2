// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "switchBase.generated.h"

UCLASS()
class FIT2097_A2TEST_API AswitchBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AswitchBase();

	//meshes
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* switchMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//switch conditions
	UPROPERTY(BlueprintReadWrite, Category = "switch control")
		bool isPointedTo;

	UPROPERTY(EditAnywhere, Category = "switch control")
		bool isEnabled;

	UPROPERTY(BlueprintReadWrite, Category = "switch control")
		bool isGlow;

	UPROPERTY(EditAnywhere, Category = "switch control")
		FString switchName;

	UPROPERTY(EditAnywhere, Category = "switch control")
		int32 switchID;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//mutators and accessors
	UFUNCTION(BlueprintCallable, Category = "switch control")
		bool getIsPointedTo();

	UFUNCTION(BlueprintCallable, Category = "switch control")
		bool getIsEnabled();

	UFUNCTION(BlueprintCallable, Category = "switch control")
		FString getSwitchName();

	UFUNCTION(BlueprintCallable, Category = "switch control")
		int32 getID();

	UFUNCTION(BlueprintCallable, Category = "switch control")
		void setIsPointedTo(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "switch control")
		void setIsEnabled(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "switch control")
		bool getIsGlow();

	UFUNCTION(BlueprintCallable, Category = "switch control")
		void setIsGlow(bool inBool);


	//blueprint overloaded functions
	//open the door
	UFUNCTION(BlueprintImplementableEvent, Category = "switch action")
		void changeSwitchStatus();

	UFUNCTION(BlueprintImplementableEvent, Category = "switch action")
		void glowSwitch();

	
};
