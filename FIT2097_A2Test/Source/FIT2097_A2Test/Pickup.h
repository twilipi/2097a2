// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS()
class FIT2097_A2TEST_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

	//meshes
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* pickupMesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = "pickup control")
		bool isPointedTo;

	UPROPERTY(BlueprintReadWrite, Category = "pickup control")
		bool isGlow;

	UPROPERTY(EditAnywhere, Category = "pickup control")
		FString pickupName;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//mutators and accessors
	UFUNCTION(BlueprintCallable, Category = "pickup control")
		FString getPickupName();

	UFUNCTION(BlueprintCallable, Category = "pickup control")
		bool getIsPointedTo();

	UFUNCTION(BlueprintCallable, Category = "pickup control")
		void setIsPointedTo(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "pickup control")
		bool getIsGlow();

	UFUNCTION(BlueprintCallable, Category = "pickup control")
		void setIsGlow(bool inBool);

	//blueprint overloaded functions
		//pickup the object and then destory the object
	UFUNCTION(BlueprintImplementableEvent, Category = "switch action")
		void pickupAction();

	UFUNCTION(BlueprintImplementableEvent, Category = "switch action")
		void pickupGlow();
	
};
