// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "TP_ThirdPersonCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// ATP_ThirdPersonCharacter

ATP_ThirdPersonCharacter::ATP_ThirdPersonCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//set level 4 switch password in default
	lv4Pass.Init(false, 3);
	currentLv4Pass.Init(false, 3);

}

//////////////////////////////////////////////////////////////////////////
// Input

void ATP_ThirdPersonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ATP_ThirdPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATP_ThirdPersonCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATP_ThirdPersonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATP_ThirdPersonCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ATP_ThirdPersonCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ATP_ThirdPersonCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ATP_ThirdPersonCharacter::OnResetVR);
}



void ATP_ThirdPersonCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ATP_ThirdPersonCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ATP_ThirdPersonCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ATP_ThirdPersonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATP_ThirdPersonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ATP_ThirdPersonCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ATP_ThirdPersonCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//***************************************************************************************************
//** Trace functions - used to detect items we are looking at in the world
//***************************************************************************************************
//***************************************************************************************************

//***************************************************************************************************
//** Trace() - called by our CallMyTrace() function which sets up our parameters and passes them through
//***************************************************************************************************


bool ATP_ThirdPersonCharacter::Trace(UWorld * World, TArray<AActor*>& ActorsToIgnore, const FVector & Start, const FVector & End, FHitResult & HitOut, ECollisionChannel CollisionChannel, bool ReturnPhysMat)
{
		// The World parameter refers to our game world (map/level) 
		// If there is no World, abort
		if (!World)
		{
			return false;
		}

		// Set up our TraceParams object
		FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);

		// Should we simple or complex collision?
		TraceParams.bTraceComplex = true;

		// We don't need Physics materials 
		TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;

		// Add our ActorsToIgnore
		TraceParams.AddIgnoredActors(ActorsToIgnore);

		// When we're debugging it is really useful to see where our trace is in the world
		// We can use World->DebugDrawTraceTag to tell Unreal to draw debug lines for our trace
		// (remove these lines to remove the debug - or better create a debug switch!)
		const FName TraceTag("MyTraceTag");
		World->DebugDrawTraceTag = TraceTag;
		TraceParams.TraceTag = TraceTag;


		// Force clear the HitData which contains our results
		HitOut = FHitResult(ForceInit);

		// Perform our trace
		World->LineTraceSingleByChannel
		(
			HitOut,		//result
			Start,	//start
			End, //end
			CollisionChannel, //collision channel
			TraceParams
		);

		// If we hit an actor, return true
		return (HitOut.GetActor() != NULL);

}

//***************************************************************************************************
//** CallMyTrace() - sets up our parameters and then calls our Trace() function
//***************************************************************************************************


void ATP_ThirdPersonCharacter::CallMyTrace()
{
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = FollowCamera->GetComponentLocation();
	const FVector ForwardVector = FollowCamera->GetForwardVector();

	// How for in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 256;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);

	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		// Process our HitData
		if (HitData.GetActor())
		{

			//UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *HitData.GetActor()->GetName());
			ProcessTraceHit(HitData);

		}
		else
		{
			// The trace did not return an Actor
			// An error has occurred
			// Record a message in the error log
		}
	}
	else
	{
		// We did not hit an Actor
		ClearPickupInfo();

	}

}

//***************************************************************************************************
//** ProcessTraceHit() - process our Trace Hit result
//***************************************************************************************************

void ATP_ThirdPersonCharacter::ProcessTraceHit(FHitResult & HitOut)
{
	// Cast the actor to APickup
	APickup* const TestPickup = Cast<APickup>(HitOut.GetActor());

	if (TestPickup)
	{
		// Keep a pointer to the Pickup
		CurrentPickup = TestPickup;

		// Set a local variable of the PickupName for the HUD
		//UE_LOG(LogClass, Warning, TEXT("PickupName: %s"), *TestPickup->getPickupName());
		FString PickupName = TestPickup->getPickupName();

		CurrentPickup->setIsPointedTo(true);

	}
	else
	{
		//UE_LOG(LogClass, Warning, TEXT("TestPickup is NOT a Pickup!"));

	}

	// Cast the actor to AdoorBase type classes
	AdoorBase* const testDoor = Cast<AdoorBase>(HitOut.GetActor());
	if (testDoor) {
		// Keep a pointer to the Pickup
		currentDoor = testDoor;

		FString doorName = testDoor->getDoorName();
		// Set a local variable of the PickupName for the HUD
	//	UE_LOG(LogClass, Warning, TEXT("PickupName: %s"), *doorName);
		currentDoor->setIsPointedTo(true);




		// Set a local variable of the PickupDisplayText for the HUD
		//		UE_LOG(LogClass, Warning, TEXT("PickupDisplayText: %s"), *TestDoor->GetPickupDisplayText());
		//		FString PickupDisplayText = TestPickup->GetPickupDisplayText();
	}
	else
	{
		//if no longer points to, clear the current switch cache into null(do it in clearPickupInfo)

		//UE_LOG(LogClass, Warning, TEXT("TestPickup is NOT a Pickup!"));
		//currentDoor->setIsPointedTo(false);
		//ClearPickupInfo();

	}

	//cast the actor to AswitchBase type classes
	AswitchBase* const testSwitch = Cast<AswitchBase>(HitOut.GetActor());
	if (testSwitch) {
		// Keep a pointer address to the current switch cache
		currentSwitch = testSwitch;

		FString switchName = testSwitch->getSwitchName();
		// Set a local variable of the PickupName for the HUD
	//	UE_LOG(LogClass, Warning, TEXT("PickupName: %s"), *switchName);
		currentSwitch->setIsPointedTo(true);




		// Set a local variable of the PickupDisplayText for the HUD
		//		UE_LOG(LogClass, Warning, TEXT("PickupDisplayText: %s"), *TestDoor->GetPickupDisplayText());
		//		FString PickupDisplayText = TestPickup->GetPickupDisplayText();
	}
	else
	{
		//if no longer points to, clear the current switch cache into null(do it in clearPickupInfo)

	}

	//cast the actor to AfusebayBase
	AfuseBayBase* const testFuseBay = Cast<AfuseBayBase>(HitOut.GetActor());
	if (testFuseBay) {
		currentFuseBay = testFuseBay;

		FString fuseBayName = testFuseBay->getFuseBayName();
		// Set a local variable of the PickupName for the HUD
		//UE_LOG(LogClass, Warning, TEXT("PickupName: %s"), *fuseBayName);

		currentFuseBay->setIsPointedTo(true);
	}
	else
	{
		//if no longer points to, clear the current switch cache into null(do it in clearPickupInfo)

	}
}

void ATP_ThirdPersonCharacter::ClearPickupInfo()
{
	//if player pointed a pickup object in the last frame, disable that object and clear the address(include is pointed to boolean)
	if (CurrentPickup) {
		CurrentPickup = NULL;
	}
	//same situation as below objects
	else if (currentDoor) {
		currentDoor->setIsPointedTo(false);
		currentDoor = NULL;
	}
	else if (currentSwitch) {
		currentSwitch->setIsPointedTo(false);
		currentSwitch = NULL;
	}
	else if (currentFuseBay) {
		currentFuseBay->setIsPointedTo(false);
		currentFuseBay = NULL;
	}
}

void ATP_ThirdPersonCharacter::clearDoorInfo()
{
	currentDoor = NULL;
}

void ATP_ThirdPersonCharacter::setDoorLockStatus(FString targetDoorName, bool targetDoorLockStatus)
{
	//find the target door name within the doorbase class actors in the game world
		//get the array firsts
	TArray<AActor*> foundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AdoorBase::StaticClass(),foundActors);
		//iterate and find the door base object
	for (AActor* loopActor:foundActors) {
		//make sure the actor is a doorbase object.
		AdoorBase* const testDoor = Cast<AdoorBase>(loopActor);
		if (testDoor) {
			//check the name is the target door
			if (testDoor->getDoorName() == targetDoorName) {
				//if the target door is already opened, ignore the process as well.
				if (!testDoor->getIsOpened()) {
					//after found the actor, set the lock status(true=lock, false=unlock)
					testDoor->setIsLocked(targetDoorLockStatus);
					//cosmetics: glow up the door for noticing.
					//if locked, set glowing status to false, otherwise set it as true
					testDoor->setIsGlow(!targetDoorLockStatus);
					testDoor->glowDoor();
				}
				//as we found the target object(either is already opened or not), break the whole loop for time saving.
				break;
			}
		}
	}
}

void ATP_ThirdPersonCharacter::setFuseBayStatus(FString targetFuseBayName, bool targetFuseBayStatus)
{
	//find the target door name within the doorbase class actors in the game world
		//get the array firsts
	TArray<AActor*> foundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AfuseBayBase::StaticClass(), foundActors);
		//iterate and find the fuseBay object
	for (AActor* loopActor : foundActors) {
		//make sure the actor is a fusebaybase object.
		AfuseBayBase* const testFuseBay = Cast<AfuseBayBase>(loopActor);
		if (testFuseBay) {
			//check the name is the target fuse base
			if (testFuseBay->getFuseBayName() == "fuse bay") {
				//if the fuse bay is already get or set the fuse, ignore the process as well.
				if (!testFuseBay->getIsFuseGet() || !testFuseBay->getIsFuseSet()) {
					//let fusebay recognise the fuse is get from player
					testFuseBay->setIsFuseGet(true);
					//and then glow the bay
					testFuseBay->setIsGlow(true);
					testFuseBay->glowBay();
					//as we found the target object(either is already opened or not), break the whole loop for time saving.
					break;
				}

			}
		}

	}


}

//process key E event, when player contact with a interactive object and press E, some kind of interaction occurs
void ATP_ThirdPersonCharacter::processEEvent()
{
	//if door is pointed
	if (currentDoor) {
		//if the pointed door is locked, open the door.
		if (!currentDoor->getIsLocked()) {
			//if the door is already open, ignore it
			if (!currentDoor->getIsOpened()) {
				currentDoor->openDoor();
				UE_LOG(LogClass, Warning, TEXT("door opens!"));
				currentDoor->setIsGlow(false);
				currentDoor->glowDoor();
			}
		}
		//otherwise notice player the door is currently closed.
		else {
			UE_LOG(LogClass, Warning, TEXT("locked!!!"));

		}
	}
	//if a switch is pointed
	else if (currentSwitch) {
		//if the switch is enabled, disable it, otherwise enable it.
		if (currentSwitch->getSwitchName() == "lv2 switch") {
			if (!currentSwitch->getIsEnabled()) {
				UE_LOG(LogClass, Warning, TEXT("%s enabled"), *currentSwitch->getSwitchName());
				currentSwitch->setIsEnabled(true);
				currentSwitch->changeSwitchStatus();
				setDoorLockStatus("door 2", false);
				UE_LOG(LogClass, Warning, TEXT("door 2 unlocked!"));

			}
			else {
				UE_LOG(LogClass, Warning, TEXT("%s disabled"), *currentSwitch->getSwitchName());
				currentSwitch->setIsEnabled(false);
				currentSwitch->changeSwitchStatus();
				setDoorLockStatus("door 2", true);
				UE_LOG(LogClass, Warning, TEXT("door 2 locked!"));


			}
		}
		//if the swich
		else if (currentSwitch->getSwitchName() == "lv4 switch") {
			//get the switch's index first
			int currentID = currentSwitch->getID();
			//change the switches' status
			if (!currentSwitch->getIsEnabled()) {
				UE_LOG(LogClass, Warning, TEXT("%s enabled"), *currentSwitch->getSwitchName());
				currentSwitch->setIsEnabled(true);
				currentSwitch->changeSwitchStatus();
			}
			else {
				UE_LOG(LogClass, Warning, TEXT("%s disabled"), *currentSwitch->getSwitchName());
				currentSwitch->setIsEnabled(false);
				currentSwitch->changeSwitchStatus();
			}
			//and then assign its index matches its to the current status array.
			currentLv4Pass[currentID] = currentSwitch->getIsEnabled();
			//and then check if all of those current set is same as lv4
			bool isIdentical = true;
			for (int32 i = 0; i != lv4Pass.Num(); i++) {
				if (lv4Pass[i] != currentLv4Pass[i]) {
					isIdentical = false;
					break;
				}
			}
			//if it's identical each other, 
			if (!isIdentical) {
				setDoorLockStatus("door 4", true);
				UE_LOG(LogClass, Warning, TEXT("door 4 still/being locked!"));
			}
			else {
				setDoorLockStatus("door 4", false);
				UE_LOG(LogClass, Warning, TEXT("door 4 unlocked!"));

			}

		}
	}
	//if a pickup item is pointed
	else if (CurrentPickup) {
		UE_LOG(LogClass, Warning, TEXT("%s is picked up"), *CurrentPickup->getPickupName());
		//if the pickup is a key, then change one of the door status.
		if (CurrentPickup->getPickupName() == "key") {
			setDoorLockStatus("door", false);
			UE_LOG(LogClass, Warning, TEXT("door 1 unlocked!"));
		}
		//if the pickup is a fuse, then change the property of the fuse bay (to make an illusion of ready to set the fuse when set.)
		else if (CurrentPickup->getPickupName() == "fuse") {
			setFuseBayStatus("fuse bay", true);
		}
		//and then del. the and do other things before being deleted.
		CurrentPickup->pickupAction();

	}
	//if a fuse bay is pointed
	else if (currentFuseBay) {
		UE_LOG(LogClass, Warning, TEXT("%s is called"), *currentFuseBay->getFuseBayName());
		if (currentFuseBay->getIsFuseGet()) {
			if (currentFuseBay->getIsFuseSet()) {
				UE_LOG(LogClass, Warning, TEXT("fuse is already set"));

			}
			else {
				UE_LOG(LogClass, Warning, TEXT("fuse is set"));
				currentFuseBay->setIsFuseSet(true);
				currentFuseBay->setIsGlow(false);
				currentFuseBay->glowBay();
				currentFuseBay->setFuse();
				setDoorLockStatus("door 3", false);
				UE_LOG(LogClass, Warning, TEXT("door 3 unlocked!"));

			}
		}
		else {
			UE_LOG(LogClass, Warning, TEXT("you haven't got the fuse."));
		}
	}
}


void ATP_ThirdPersonCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	CurrentPickup = NULL;
	currentDoor = NULL;
	currentSwitch = NULL;

}

void ATP_ThirdPersonCharacter::Tick(float DeltaSeconds)
{
	CallMyTrace();

}