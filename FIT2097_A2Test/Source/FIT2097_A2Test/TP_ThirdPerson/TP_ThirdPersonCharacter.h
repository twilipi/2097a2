// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Pickup.h"
#include "doorBase.h"
#include "switchBase.h"
#include "fuseBayBase.h"
#include <string>
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "TP_ThirdPersonCharacter.generated.h"

UCLASS(config=Game)
class ATP_ThirdPersonCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	ATP_ThirdPersonCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay();

	void Tick(float DeltaSeconds) override;



public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	//***************************************************************************************************
	//** Trace functions - used to detect items we are looking at in the world
	//** Adapted from code found on the unreal wiki https://wiki.unrealengine.com/Trace_Functions
	//***************************************************************************************************

	bool Trace(
		UWorld* World,
		TArray<AActor*>& ActorsToIgnore,
		const FVector& Start,
		const FVector& End,
		FHitResult& HitOut,
		ECollisionChannel CollisionChannel,
		bool ReturnPhysMat
	);

	void CallMyTrace();

	void ProcessTraceHit(FHitResult& HitOut);

	//temp pointers when being pointed
	UPROPERTY(BlueprintReadOnly, Category = "pickup parts")
		APickup* CurrentPickup;

	UPROPERTY(BlueprintReadOnly, Category = "pickup parts")
		AdoorBase* currentDoor;

	UPROPERTY(BlueprintReadOnly, Category = "pickup parts")
		AswitchBase* currentSwitch;

	UPROPERTY(BlueprintReadOnly, Category = "pickup parts")
		AfuseBayBase* currentFuseBay;

	//the preset password of level 4 switches
	UPROPERTY(EditAnywhere,Category="level 4 password" )
		TArray<bool> lv4Pass;
	//current status of level 4 switches
	UPROPERTY(EditAnywhere, Category = "level 4 password")
		TArray<bool> currentLv4Pass;





	//clear pointable object info
	void ClearPickupInfo();
	void clearDoorInfo();

	//change door's lock status using specfied keyword
	UFUNCTION(BlueprintCallable, Category = "change pickups statuses")
		void setDoorLockStatus(FString targetDoorName, bool targetDoorLockStatus);

	//take action when player picked the fuse
	UFUNCTION(BlueprintCallable, Category = "change pickups statuses")
		void setFuseBayStatus(FString targetFuseBayName, bool targetFuseBayStatus);


	//BP event: action when E is pressed, make action based on door's status.
	UFUNCTION(BlueprintCallable, Category = "take actions")
		void processEEvent();

};

