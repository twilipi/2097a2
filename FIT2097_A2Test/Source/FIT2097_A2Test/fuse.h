// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "fuse.generated.h"

/**
 * 
 */
UCLASS()
class FIT2097_A2TEST_API Afuse : public APickup
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties

protected:
	virtual void BeginPlay() override;

	
};
