// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"


// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	pickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("switch"));
	RootComponent = pickupMesh;

	isPointedTo = false;
	isGlow = false;
	pickupName = "pickup item";

	bReplicates = true;
	bReplicateMovement = true;
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();

	pickupGlow();

	
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString APickup::getPickupName()
{
	return pickupName;
}

bool APickup::getIsPointedTo()
{
	return isPointedTo;
}

void APickup::setIsPointedTo(bool inBool)
{
	isPointedTo = inBool;
}

bool APickup::getIsGlow()
{
	return isGlow;
}

void APickup::setIsGlow(bool inBool)
{
	isGlow = inBool;
}


