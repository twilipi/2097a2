// Fill out your copyright notice in the Description page of Project Settings.

#include "testDoor.h"


// Sets default values
AtestDoor::AtestDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	doorFrame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door frame"));
	RootComponent = doorFrame;

	door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door"));
	door->SetupAttachment(RootComponent);



}

// Called when the game starts or when spawned
void AtestDoor::BeginPlay()
{
	Super::BeginPlay();
//	this->displayString("12333");
	isPointedTo = false;
	isLocked = false;	
}

// Called every frame
void AtestDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AtestDoor::getIsPointedTo()
{
	return isPointedTo;
}

bool AtestDoor::getIsLocked()
{
	return isLocked;
}

void AtestDoor::setIsPointedTo(bool inBool)
{
	isPointedTo = inBool;
}

void AtestDoor::setIsLocked(bool inBool)
{
	isLocked = inBool;
}

FString AtestDoor::GetPickupName()
{
	return "I point the door";

}


//void AtestDoor::displayString(FString inString)
//{
//}
//
//void AtestDoor::displayString_Implementation(FString inString)
//{
//}

