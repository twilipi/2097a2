// Fill out your copyright notice in the Description page of Project Settings.

#include "fuseBayBase.h"


// Sets default values
AfuseBayBase::AfuseBayBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	fuseBay = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door frame"));
	RootComponent = fuseBay;

	fuseCore = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("door"));
	fuseCore->SetupAttachment(RootComponent);


	isPointedTo = false;
	isFuseGet = false;
	isFuseSet = false;
	isGlow = false;
	fuseBayName = "fuse bay";

	bReplicates = true;
	bReplicateMovement = true;


}

// Called when the game starts or when spawned
void AfuseBayBase::BeginPlay()
{
	Super::BeginPlay();

	//the fuse core is defaulted as invisible.
	//fuseCore->ToggleVisibility(false);
	fuseCore->SetVisibility(false);

	glowBay();
	
}

// Called every frame
void AfuseBayBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString AfuseBayBase::getFuseBayName()
{
	return fuseBayName;
}

bool AfuseBayBase::getIsPointedTo()
{
	return isPointedTo;
}

void AfuseBayBase::setIsPointedTo(bool inBool)
{
	isPointedTo = inBool;
}

bool AfuseBayBase::getIsGlow()
{
	return isGlow;
}

void AfuseBayBase::setIsGlow(bool inBool)
{
	isGlow = inBool;
}

bool AfuseBayBase::getIsFuseGet()
{
	return isFuseGet;
}

void AfuseBayBase::setIsFuseGet(bool inBool)
{
	isFuseGet = inBool;
}

bool AfuseBayBase::getIsFuseSet()
{
	return isFuseSet;
}

void AfuseBayBase::setIsFuseSet(bool inBool)
{
	isFuseSet = inBool;
}

void AfuseBayBase::setFuse()
{
	//fuseCore->ToggleVisibility(true);
	fuseCore->SetVisibility(true);

	isFuseSet = true;
	glowFuse();
}



