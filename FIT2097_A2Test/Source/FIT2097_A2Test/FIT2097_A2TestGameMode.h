// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FIT2097_A2TestGameMode.generated.h"

UCLASS(minimalapi)
class AFIT2097_A2TestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFIT2097_A2TestGameMode();
};



