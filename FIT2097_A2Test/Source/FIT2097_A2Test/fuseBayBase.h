// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "fuseBayBase.generated.h"

UCLASS()
class FIT2097_A2TEST_API AfuseBayBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AfuseBayBase();

	//meshes
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* fuseBay;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* fuseCore;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = "fuse bay control")
		bool isPointedTo;

	UPROPERTY(EditAnywhere, Category = "fuse bay control")
		bool isFuseGet;

	UPROPERTY(EditAnywhere, Category = "fuse bay control")
		bool isFuseSet;

	UPROPERTY(EditAnywhere, Category = "fuse bay control")
		bool isGlow;

	UPROPERTY(EditAnywhere, Category = "fuse bay control")
		FString fuseBayName;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//mutators and accessors
	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		FString getFuseBayName();

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		bool getIsPointedTo();

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		void setIsPointedTo(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		bool getIsGlow();

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		void setIsGlow(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		bool getIsFuseGet();

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		void setIsFuseGet(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		bool getIsFuseSet();

	UFUNCTION(BlueprintCallable, Category = "fuse bay control")
		void setIsFuseSet(bool inBool);

	//set up the fuse if fuse is get
	UFUNCTION(BlueprintCallable, Category = "fuse bay action")
		void setFuse();

	//blueprint overloaded functions


	UFUNCTION(BlueprintImplementableEvent, Category = "fuse bay action")
		void glowFuse();

	UFUNCTION(BlueprintImplementableEvent, Category = "fuse bay action")
		void glowBay();



	
	
};
