// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "doorBase.generated.h"

UCLASS()
class FIT2097_A2TEST_API AdoorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AdoorBase();

	//meshes
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* doorFrame;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		UStaticMeshComponent* door;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	//door conditions
	UPROPERTY(BlueprintReadWrite, Category = "door control")
		bool isPointedTo;

	UPROPERTY(EditAnywhere, Category = "door control")
		bool isGlow;

	UPROPERTY(EditAnywhere, Category = "door control")
		bool isLocked;

	UPROPERTY(EditAnywhere, Category = "door control")
		bool isOpened;

	UPROPERTY(EditAnywhere, Category = "door control")
		FString doorName;




public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//mutators and accessors
	UFUNCTION(BlueprintCallable, Category = "door control")
		bool getIsPointedTo();

	UFUNCTION(BlueprintCallable, Category = "door control")
		bool getIsLocked();

	UFUNCTION(BlueprintCallable, Category = "door control")
		bool getIsOpened();

	UFUNCTION(BlueprintCallable, Category = "door control")
		void setIsPointedTo(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "door control")
		void setIsLocked(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "door control")
		void setIsOpened(bool inBool);

	UFUNCTION(BlueprintCallable, Category = "door control")
		FString getDoorName();

	UFUNCTION(BlueprintCallable, Category = "door control")
		bool getIsGlow();

	UFUNCTION(BlueprintCallable, Category = "door control")
		void setIsGlow(bool inBool);


	//blueprint overloaded functions
		//open the door
	UFUNCTION(BlueprintImplementableEvent, Category = "door action")
		void openDoor();

	UFUNCTION(BlueprintImplementableEvent, Category = "door action")
		void glowDoor();


	
};
