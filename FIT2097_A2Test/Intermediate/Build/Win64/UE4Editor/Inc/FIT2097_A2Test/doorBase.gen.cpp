// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "doorBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodedoorBase() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AdoorBase_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AdoorBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_getDoorName();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_getIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_getIsLocked();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_getIsOpened();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_getIsPointedTo();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_glowDoor();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_openDoor();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_setIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_setIsLocked();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_setIsOpened();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AdoorBase_setIsPointedTo();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AdoorBase_glowDoor = FName(TEXT("glowDoor"));
	void AdoorBase::glowDoor()
	{
		ProcessEvent(FindFunctionChecked(NAME_AdoorBase_glowDoor),NULL);
	}
	static FName NAME_AdoorBase_openDoor = FName(TEXT("openDoor"));
	void AdoorBase::openDoor()
	{
		ProcessEvent(FindFunctionChecked(NAME_AdoorBase_openDoor),NULL);
	}
	void AdoorBase::StaticRegisterNativesAdoorBase()
	{
		UClass* Class = AdoorBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "getDoorName", &AdoorBase::execgetDoorName },
			{ "getIsGlow", &AdoorBase::execgetIsGlow },
			{ "getIsLocked", &AdoorBase::execgetIsLocked },
			{ "getIsOpened", &AdoorBase::execgetIsOpened },
			{ "getIsPointedTo", &AdoorBase::execgetIsPointedTo },
			{ "setIsGlow", &AdoorBase::execsetIsGlow },
			{ "setIsLocked", &AdoorBase::execsetIsLocked },
			{ "setIsOpened", &AdoorBase::execsetIsOpened },
			{ "setIsPointedTo", &AdoorBase::execsetIsPointedTo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AdoorBase_getDoorName()
	{
		struct doorBase_eventgetDoorName_Parms
		{
			FString ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(doorBase_eventgetDoorName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "getDoorName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventgetDoorName_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_getIsGlow()
	{
		struct doorBase_eventgetIsGlow_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((doorBase_eventgetIsGlow_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventgetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "getIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventgetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_getIsLocked()
	{
		struct doorBase_eventgetIsLocked_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((doorBase_eventgetIsLocked_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventgetIsLocked_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "getIsLocked", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventgetIsLocked_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_getIsOpened()
	{
		struct doorBase_eventgetIsOpened_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((doorBase_eventgetIsOpened_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventgetIsOpened_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "getIsOpened", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventgetIsOpened_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_getIsPointedTo()
	{
		struct doorBase_eventgetIsPointedTo_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((doorBase_eventgetIsPointedTo_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventgetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
				{ "ToolTip", "mutators and accessors" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "getIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventgetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_glowDoor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door action" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "glowDoor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_openDoor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door action" },
				{ "ModuleRelativePath", "doorBase.h" },
				{ "ToolTip", "blueprint overloaded functions\nopen the door" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "openDoor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_setIsGlow()
	{
		struct doorBase_eventsetIsGlow_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((doorBase_eventsetIsGlow_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventsetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "setIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventsetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_setIsLocked()
	{
		struct doorBase_eventsetIsLocked_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((doorBase_eventsetIsLocked_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventsetIsLocked_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "setIsLocked", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventsetIsLocked_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_setIsOpened()
	{
		struct doorBase_eventsetIsOpened_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((doorBase_eventsetIsOpened_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventsetIsOpened_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "setIsOpened", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventsetIsOpened_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AdoorBase_setIsPointedTo()
	{
		struct doorBase_eventsetIsPointedTo_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((doorBase_eventsetIsPointedTo_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(doorBase_eventsetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AdoorBase, "setIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(doorBase_eventsetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AdoorBase_NoRegister()
	{
		return AdoorBase::StaticClass();
	}
	UClass* Z_Construct_UClass_AdoorBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AdoorBase_getDoorName, "getDoorName" }, // 2474715591
				{ &Z_Construct_UFunction_AdoorBase_getIsGlow, "getIsGlow" }, // 1920034615
				{ &Z_Construct_UFunction_AdoorBase_getIsLocked, "getIsLocked" }, // 4046512036
				{ &Z_Construct_UFunction_AdoorBase_getIsOpened, "getIsOpened" }, // 2967114961
				{ &Z_Construct_UFunction_AdoorBase_getIsPointedTo, "getIsPointedTo" }, // 683626225
				{ &Z_Construct_UFunction_AdoorBase_glowDoor, "glowDoor" }, // 1106642913
				{ &Z_Construct_UFunction_AdoorBase_openDoor, "openDoor" }, // 2555101711
				{ &Z_Construct_UFunction_AdoorBase_setIsGlow, "setIsGlow" }, // 2698160592
				{ &Z_Construct_UFunction_AdoorBase_setIsLocked, "setIsLocked" }, // 2879323974
				{ &Z_Construct_UFunction_AdoorBase_setIsOpened, "setIsOpened" }, // 1380869850
				{ &Z_Construct_UFunction_AdoorBase_setIsPointedTo, "setIsPointedTo" }, // 3645043099
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "doorBase.h" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorName_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_doorName = { UE4CodeGen_Private::EPropertyClass::Str, "doorName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, STRUCT_OFFSET(AdoorBase, doorName), METADATA_PARAMS(NewProp_doorName_MetaData, ARRAY_COUNT(NewProp_doorName_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isOpened_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			auto NewProp_isOpened_SetBit = [](void* Obj){ ((AdoorBase*)Obj)->isOpened = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isOpened = { UE4CodeGen_Private::EPropertyClass::Bool, "isOpened", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AdoorBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isOpened_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isOpened_MetaData, ARRAY_COUNT(NewProp_isOpened_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isLocked_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			auto NewProp_isLocked_SetBit = [](void* Obj){ ((AdoorBase*)Obj)->isLocked = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isLocked = { UE4CodeGen_Private::EPropertyClass::Bool, "isLocked", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AdoorBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isLocked_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isLocked_MetaData, ARRAY_COUNT(NewProp_isLocked_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isGlow_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			auto NewProp_isGlow_SetBit = [](void* Obj){ ((AdoorBase*)Obj)->isGlow = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isGlow = { UE4CodeGen_Private::EPropertyClass::Bool, "isGlow", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AdoorBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isGlow_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isGlow_MetaData, ARRAY_COUNT(NewProp_isGlow_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isPointedTo_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "doorBase.h" },
				{ "ToolTip", "door conditions" },
			};
#endif
			auto NewProp_isPointedTo_SetBit = [](void* Obj){ ((AdoorBase*)Obj)->isPointedTo = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isPointedTo = { UE4CodeGen_Private::EPropertyClass::Bool, "isPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AdoorBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isPointedTo_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isPointedTo_MetaData, ARRAY_COUNT(NewProp_isPointedTo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_door_MetaData[] = {
				{ "Category", "doorBase" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "doorBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_door = { UE4CodeGen_Private::EPropertyClass::Object, "door", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AdoorBase, door), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_door_MetaData, ARRAY_COUNT(NewProp_door_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorFrame_MetaData[] = {
				{ "Category", "doorBase" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "doorBase.h" },
				{ "ToolTip", "meshes" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_doorFrame = { UE4CodeGen_Private::EPropertyClass::Object, "doorFrame", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AdoorBase, doorFrame), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_doorFrame_MetaData, ARRAY_COUNT(NewProp_doorFrame_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_doorName,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isOpened,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isLocked,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isGlow,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isPointedTo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_door,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_doorFrame,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AdoorBase>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AdoorBase::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AdoorBase, 744166752);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AdoorBase(Z_Construct_UClass_AdoorBase, &AdoorBase::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("AdoorBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AdoorBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
