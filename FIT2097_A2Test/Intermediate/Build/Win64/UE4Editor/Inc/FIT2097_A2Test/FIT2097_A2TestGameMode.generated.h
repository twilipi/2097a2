// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_FIT2097_A2TestGameMode_generated_h
#error "FIT2097_A2TestGameMode.generated.h already included, missing '#pragma once' in FIT2097_A2TestGameMode.h"
#endif
#define FIT2097_A2TEST_FIT2097_A2TestGameMode_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_RPC_WRAPPERS
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFIT2097_A2TestGameMode(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AFIT2097_A2TestGameMode(); \
public: \
	DECLARE_CLASS(AFIT2097_A2TestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/FIT2097_A2Test"), FIT2097_A2TEST_API) \
	DECLARE_SERIALIZER(AFIT2097_A2TestGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFIT2097_A2TestGameMode(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AFIT2097_A2TestGameMode(); \
public: \
	DECLARE_CLASS(AFIT2097_A2TestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/FIT2097_A2Test"), FIT2097_A2TEST_API) \
	DECLARE_SERIALIZER(AFIT2097_A2TestGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FIT2097_A2TEST_API AFIT2097_A2TestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT2097_A2TestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIT2097_A2TEST_API, AFIT2097_A2TestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097_A2TestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIT2097_A2TEST_API AFIT2097_A2TestGameMode(AFIT2097_A2TestGameMode&&); \
	FIT2097_A2TEST_API AFIT2097_A2TestGameMode(const AFIT2097_A2TestGameMode&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIT2097_A2TEST_API AFIT2097_A2TestGameMode(AFIT2097_A2TestGameMode&&); \
	FIT2097_A2TEST_API AFIT2097_A2TestGameMode(const AFIT2097_A2TestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIT2097_A2TEST_API, AFIT2097_A2TestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097_A2TestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFIT2097_A2TestGameMode)


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_9_PROLOG
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
