// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_TP_ThirdPersonCharacter_generated_h
#error "TP_ThirdPersonCharacter.generated.h already included, missing '#pragma once' in TP_ThirdPersonCharacter.h"
#endif
#define FIT2097_A2TEST_TP_ThirdPersonCharacter_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execprocessEEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->processEEvent(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetFuseBayStatus) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_targetFuseBayName); \
		P_GET_UBOOL(Z_Param_targetFuseBayStatus); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setFuseBayStatus(Z_Param_targetFuseBayName,Z_Param_targetFuseBayStatus); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetDoorLockStatus) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_targetDoorName); \
		P_GET_UBOOL(Z_Param_targetDoorLockStatus); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setDoorLockStatus(Z_Param_targetDoorName,Z_Param_targetDoorLockStatus); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execprocessEEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->processEEvent(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetFuseBayStatus) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_targetFuseBayName); \
		P_GET_UBOOL(Z_Param_targetFuseBayStatus); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setFuseBayStatus(Z_Param_targetFuseBayName,Z_Param_targetFuseBayStatus); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetDoorLockStatus) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_targetDoorName); \
		P_GET_UBOOL(Z_Param_targetDoorLockStatus); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setDoorLockStatus(Z_Param_targetDoorName,Z_Param_targetDoorLockStatus); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATP_ThirdPersonCharacter(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_ATP_ThirdPersonCharacter(); \
public: \
	DECLARE_CLASS(ATP_ThirdPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(ATP_ThirdPersonCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_INCLASS \
private: \
	static void StaticRegisterNativesATP_ThirdPersonCharacter(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_ATP_ThirdPersonCharacter(); \
public: \
	DECLARE_CLASS(ATP_ThirdPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(ATP_ThirdPersonCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATP_ThirdPersonCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATP_ThirdPersonCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATP_ThirdPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_ThirdPersonCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATP_ThirdPersonCharacter(ATP_ThirdPersonCharacter&&); \
	NO_API ATP_ThirdPersonCharacter(const ATP_ThirdPersonCharacter&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATP_ThirdPersonCharacter(ATP_ThirdPersonCharacter&&); \
	NO_API ATP_ThirdPersonCharacter(const ATP_ThirdPersonCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATP_ThirdPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_ThirdPersonCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATP_ThirdPersonCharacter)


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATP_ThirdPersonCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(ATP_ThirdPersonCharacter, FollowCamera); }


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_15_PROLOG
#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_TP_ThirdPerson_TP_ThirdPersonCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
