// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_switchBase_generated_h
#error "switchBase.generated.h already included, missing '#pragma once' in switchBase.h"
#endif
#define FIT2097_A2TEST_switchBase_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execsetIsGlow) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsGlow(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsGlow) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsGlow(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsEnabled) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsEnabled(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsPointedTo) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsPointedTo(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetID) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->getID(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSwitchName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->getSwitchName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsEnabled) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsEnabled(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsPointedTo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsPointedTo(); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execsetIsGlow) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsGlow(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsGlow) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsGlow(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsEnabled) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsEnabled(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsPointedTo) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsPointedTo(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetID) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->getID(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSwitchName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->getSwitchName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsEnabled) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsEnabled(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsPointedTo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsPointedTo(); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_EVENT_PARMS
#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_CALLBACK_WRAPPERS
#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAswitchBase(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AswitchBase(); \
public: \
	DECLARE_CLASS(AswitchBase, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AswitchBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAswitchBase(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AswitchBase(); \
public: \
	DECLARE_CLASS(AswitchBase, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AswitchBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AswitchBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AswitchBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AswitchBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AswitchBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AswitchBase(AswitchBase&&); \
	NO_API AswitchBase(const AswitchBase&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AswitchBase(AswitchBase&&); \
	NO_API AswitchBase(const AswitchBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AswitchBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AswitchBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AswitchBase)


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__isPointedTo() { return STRUCT_OFFSET(AswitchBase, isPointedTo); } \
	FORCEINLINE static uint32 __PPO__isEnabled() { return STRUCT_OFFSET(AswitchBase, isEnabled); } \
	FORCEINLINE static uint32 __PPO__isGlow() { return STRUCT_OFFSET(AswitchBase, isGlow); } \
	FORCEINLINE static uint32 __PPO__switchName() { return STRUCT_OFFSET(AswitchBase, switchName); } \
	FORCEINLINE static uint32 __PPO__switchID() { return STRUCT_OFFSET(AswitchBase, switchID); }


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_9_PROLOG \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_EVENT_PARMS


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_CALLBACK_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_CALLBACK_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_switchBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
