// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "TP_ThirdPerson/TP_ThirdPersonCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTP_ThirdPersonCharacter() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_ATP_ThirdPersonCharacter_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_ATP_ThirdPersonCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_ATP_ThirdPersonCharacter_processEEvent();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_ATP_ThirdPersonCharacter_setDoorLockStatus();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_ATP_ThirdPersonCharacter_setFuseBayStatus();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AfuseBayBase_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AswitchBase_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AdoorBase_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_APickup_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
// End Cross Module References
	void ATP_ThirdPersonCharacter::StaticRegisterNativesATP_ThirdPersonCharacter()
	{
		UClass* Class = ATP_ThirdPersonCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "processEEvent", &ATP_ThirdPersonCharacter::execprocessEEvent },
			{ "setDoorLockStatus", &ATP_ThirdPersonCharacter::execsetDoorLockStatus },
			{ "setFuseBayStatus", &ATP_ThirdPersonCharacter::execsetFuseBayStatus },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_ATP_ThirdPersonCharacter_processEEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "take actions" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "BP event: action when E is pressed, make action based on door's status." },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ATP_ThirdPersonCharacter, "processEEvent", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ATP_ThirdPersonCharacter_setDoorLockStatus()
	{
		struct TP_ThirdPersonCharacter_eventsetDoorLockStatus_Parms
		{
			FString targetDoorName;
			bool targetDoorLockStatus;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_targetDoorLockStatus_SetBit = [](void* Obj){ ((TP_ThirdPersonCharacter_eventsetDoorLockStatus_Parms*)Obj)->targetDoorLockStatus = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_targetDoorLockStatus = { UE4CodeGen_Private::EPropertyClass::Bool, "targetDoorLockStatus", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(TP_ThirdPersonCharacter_eventsetDoorLockStatus_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_targetDoorLockStatus_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_targetDoorName = { UE4CodeGen_Private::EPropertyClass::Str, "targetDoorName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(TP_ThirdPersonCharacter_eventsetDoorLockStatus_Parms, targetDoorName), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_targetDoorLockStatus,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_targetDoorName,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "change pickups statuses" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "change door's lock status using specfied keyword" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ATP_ThirdPersonCharacter, "setDoorLockStatus", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(TP_ThirdPersonCharacter_eventsetDoorLockStatus_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ATP_ThirdPersonCharacter_setFuseBayStatus()
	{
		struct TP_ThirdPersonCharacter_eventsetFuseBayStatus_Parms
		{
			FString targetFuseBayName;
			bool targetFuseBayStatus;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_targetFuseBayStatus_SetBit = [](void* Obj){ ((TP_ThirdPersonCharacter_eventsetFuseBayStatus_Parms*)Obj)->targetFuseBayStatus = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_targetFuseBayStatus = { UE4CodeGen_Private::EPropertyClass::Bool, "targetFuseBayStatus", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(TP_ThirdPersonCharacter_eventsetFuseBayStatus_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_targetFuseBayStatus_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_targetFuseBayName = { UE4CodeGen_Private::EPropertyClass::Str, "targetFuseBayName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(TP_ThirdPersonCharacter_eventsetFuseBayStatus_Parms, targetFuseBayName), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_targetFuseBayStatus,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_targetFuseBayName,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "change pickups statuses" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "take action when player picked the fuse" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ATP_ThirdPersonCharacter, "setFuseBayStatus", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(TP_ThirdPersonCharacter_eventsetFuseBayStatus_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATP_ThirdPersonCharacter_NoRegister()
	{
		return ATP_ThirdPersonCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_ATP_ThirdPersonCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_ACharacter,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_ATP_ThirdPersonCharacter_processEEvent, "processEEvent" }, // 2928495130
				{ &Z_Construct_UFunction_ATP_ThirdPersonCharacter_setDoorLockStatus, "setDoorLockStatus" }, // 1594451036
				{ &Z_Construct_UFunction_ATP_ThirdPersonCharacter_setFuseBayStatus, "setFuseBayStatus" }, // 223793164
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Navigation" },
				{ "IncludePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentLv4Pass_MetaData[] = {
				{ "Category", "level 4 password" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "current status of level 4 switches" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_currentLv4Pass = { UE4CodeGen_Private::EPropertyClass::Array, "currentLv4Pass", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, currentLv4Pass), METADATA_PARAMS(NewProp_currentLv4Pass_MetaData, ARRAY_COUNT(NewProp_currentLv4Pass_MetaData)) };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_currentLv4Pass_Inner = { UE4CodeGen_Private::EPropertyClass::Bool, "currentLv4Pass", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_lv4Pass_MetaData[] = {
				{ "Category", "level 4 password" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "the preset password of level 4 switches" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_lv4Pass = { UE4CodeGen_Private::EPropertyClass::Array, "lv4Pass", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, lv4Pass), METADATA_PARAMS(NewProp_lv4Pass_MetaData, ARRAY_COUNT(NewProp_lv4Pass_MetaData)) };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_lv4Pass_Inner = { UE4CodeGen_Private::EPropertyClass::Bool, "lv4Pass", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentFuseBay_MetaData[] = {
				{ "Category", "pickup parts" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentFuseBay = { UE4CodeGen_Private::EPropertyClass::Object, "currentFuseBay", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000014, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, currentFuseBay), Z_Construct_UClass_AfuseBayBase_NoRegister, METADATA_PARAMS(NewProp_currentFuseBay_MetaData, ARRAY_COUNT(NewProp_currentFuseBay_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentSwitch_MetaData[] = {
				{ "Category", "pickup parts" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentSwitch = { UE4CodeGen_Private::EPropertyClass::Object, "currentSwitch", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000014, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, currentSwitch), Z_Construct_UClass_AswitchBase_NoRegister, METADATA_PARAMS(NewProp_currentSwitch_MetaData, ARRAY_COUNT(NewProp_currentSwitch_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_currentDoor_MetaData[] = {
				{ "Category", "pickup parts" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_currentDoor = { UE4CodeGen_Private::EPropertyClass::Object, "currentDoor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000014, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, currentDoor), Z_Construct_UClass_AdoorBase_NoRegister, METADATA_PARAMS(NewProp_currentDoor_MetaData, ARRAY_COUNT(NewProp_currentDoor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentPickup_MetaData[] = {
				{ "Category", "pickup parts" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "temp pointers when being pointed" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentPickup = { UE4CodeGen_Private::EPropertyClass::Object, "CurrentPickup", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000014, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, CurrentPickup), Z_Construct_UClass_APickup_NoRegister, METADATA_PARAMS(NewProp_CurrentPickup_MetaData, ARRAY_COUNT(NewProp_CurrentPickup_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[] = {
				{ "Category", "Camera" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseLookUpRate", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000020015, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, BaseLookUpRate), METADATA_PARAMS(NewProp_BaseLookUpRate_MetaData, ARRAY_COUNT(NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[] = {
				{ "Category", "Camera" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate = { UE4CodeGen_Private::EPropertyClass::Float, "BaseTurnRate", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000020015, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, BaseTurnRate), METADATA_PARAMS(NewProp_BaseTurnRate_MetaData, ARRAY_COUNT(NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[] = {
				{ "AllowPrivateAccess", "true" },
				{ "Category", "Camera" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "Follow camera" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera = { UE4CodeGen_Private::EPropertyClass::Object, "FollowCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(NewProp_FollowCamera_MetaData, ARRAY_COUNT(NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[] = {
				{ "AllowPrivateAccess", "true" },
				{ "Category", "Camera" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "TP_ThirdPerson/TP_ThirdPersonCharacter.h" },
				{ "ToolTip", "Camera boom positioning the camera behind the character" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom = { UE4CodeGen_Private::EPropertyClass::Object, "CameraBoom", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a001d, 1, nullptr, STRUCT_OFFSET(ATP_ThirdPersonCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(NewProp_CameraBoom_MetaData, ARRAY_COUNT(NewProp_CameraBoom_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentLv4Pass,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentLv4Pass_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_lv4Pass,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_lv4Pass_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentFuseBay,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentSwitch,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_currentDoor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CurrentPickup,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_BaseLookUpRate,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_BaseTurnRate,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FollowCamera,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CameraBoom,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ATP_ThirdPersonCharacter>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ATP_ThirdPersonCharacter::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00800080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATP_ThirdPersonCharacter, 2370890749);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATP_ThirdPersonCharacter(Z_Construct_UClass_ATP_ThirdPersonCharacter, &ATP_ThirdPersonCharacter::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("ATP_ThirdPersonCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATP_ThirdPersonCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
