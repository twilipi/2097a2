// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_fuse_generated_h
#error "fuse.generated.h already included, missing '#pragma once' in fuse.h"
#endif
#define FIT2097_A2TEST_fuse_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_RPC_WRAPPERS
#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAfuse(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_Afuse(); \
public: \
	DECLARE_CLASS(Afuse, APickup, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(Afuse) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAfuse(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_Afuse(); \
public: \
	DECLARE_CLASS(Afuse, APickup, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(Afuse) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Afuse(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Afuse) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Afuse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Afuse); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Afuse(Afuse&&); \
	NO_API Afuse(const Afuse&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Afuse() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Afuse(Afuse&&); \
	NO_API Afuse(const Afuse&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Afuse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Afuse); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Afuse)


#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_PRIVATE_PROPERTY_OFFSET
#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_12_PROLOG
#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_fuse_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
