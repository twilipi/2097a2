// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "testDoor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodetestDoor() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AtestDoor_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AtestDoor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AtestDoor_getIsLocked();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AtestDoor_getIsPointedTo();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AtestDoor_GetPickupName();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AtestDoor_openDoor();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AtestDoor_setIsLocked();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AtestDoor_setIsPointedTo();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AtestDoor_openDoor = FName(TEXT("openDoor"));
	void AtestDoor::openDoor()
	{
		ProcessEvent(FindFunctionChecked(NAME_AtestDoor_openDoor),NULL);
	}
	void AtestDoor::StaticRegisterNativesAtestDoor()
	{
		UClass* Class = AtestDoor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "getIsLocked", &AtestDoor::execgetIsLocked },
			{ "getIsPointedTo", &AtestDoor::execgetIsPointedTo },
			{ "GetPickupName", &AtestDoor::execGetPickupName },
			{ "setIsLocked", &AtestDoor::execsetIsLocked },
			{ "setIsPointedTo", &AtestDoor::execsetIsPointedTo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AtestDoor_getIsLocked()
	{
		struct testDoor_eventgetIsLocked_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((testDoor_eventgetIsLocked_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(testDoor_eventgetIsLocked_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AtestDoor, "getIsLocked", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(testDoor_eventgetIsLocked_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AtestDoor_getIsPointedTo()
	{
		struct testDoor_eventgetIsPointedTo_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((testDoor_eventgetIsPointedTo_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(testDoor_eventgetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AtestDoor, "getIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(testDoor_eventgetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AtestDoor_GetPickupName()
	{
		struct testDoor_eventGetPickupName_Parms
		{
			FString ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(testDoor_eventGetPickupName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AtestDoor, "GetPickupName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(testDoor_eventGetPickupName_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AtestDoor_openDoor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AtestDoor, "openDoor", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AtestDoor_setIsLocked()
	{
		struct testDoor_eventsetIsLocked_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((testDoor_eventsetIsLocked_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(testDoor_eventsetIsLocked_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AtestDoor, "setIsLocked", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(testDoor_eventsetIsLocked_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AtestDoor_setIsPointedTo()
	{
		struct testDoor_eventsetIsPointedTo_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((testDoor_eventsetIsPointedTo_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(testDoor_eventsetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AtestDoor, "setIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(testDoor_eventsetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AtestDoor_NoRegister()
	{
		return AtestDoor::StaticClass();
	}
	UClass* Z_Construct_UClass_AtestDoor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AtestDoor_getIsLocked, "getIsLocked" }, // 2089864772
				{ &Z_Construct_UFunction_AtestDoor_getIsPointedTo, "getIsPointedTo" }, // 3104418089
				{ &Z_Construct_UFunction_AtestDoor_GetPickupName, "GetPickupName" }, // 709969321
				{ &Z_Construct_UFunction_AtestDoor_openDoor, "openDoor" }, // 4009945326
				{ &Z_Construct_UFunction_AtestDoor_setIsLocked, "setIsLocked" }, // 193942556
				{ &Z_Construct_UFunction_AtestDoor_setIsPointedTo, "setIsPointedTo" }, // 2777691099
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "testDoor.h" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isLocked_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			auto NewProp_isLocked_SetBit = [](void* Obj){ ((AtestDoor*)Obj)->isLocked = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isLocked = { UE4CodeGen_Private::EPropertyClass::Bool, "isLocked", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AtestDoor), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isLocked_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isLocked_MetaData, ARRAY_COUNT(NewProp_isLocked_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isPointedTo_MetaData[] = {
				{ "Category", "door control" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			auto NewProp_isPointedTo_SetBit = [](void* Obj){ ((AtestDoor*)Obj)->isPointedTo = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isPointedTo = { UE4CodeGen_Private::EPropertyClass::Bool, "isPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AtestDoor), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isPointedTo_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isPointedTo_MetaData, ARRAY_COUNT(NewProp_isPointedTo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_door_MetaData[] = {
				{ "Category", "testDoor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "testDoor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_door = { UE4CodeGen_Private::EPropertyClass::Object, "door", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AtestDoor, door), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_door_MetaData, ARRAY_COUNT(NewProp_door_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_doorFrame_MetaData[] = {
				{ "Category", "testDoor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "testDoor.h" },
				{ "ToolTip", "meshes" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_doorFrame = { UE4CodeGen_Private::EPropertyClass::Object, "doorFrame", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AtestDoor, doorFrame), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_doorFrame_MetaData, ARRAY_COUNT(NewProp_doorFrame_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isLocked,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isPointedTo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_door,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_doorFrame,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AtestDoor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AtestDoor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AtestDoor, 2788322230);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AtestDoor(Z_Construct_UClass_AtestDoor, &AtestDoor::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("AtestDoor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AtestDoor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
