// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "fuse.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodefuse() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_Afuse_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_Afuse();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_APickup();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
// End Cross Module References
	void Afuse::StaticRegisterNativesAfuse()
	{
	}
	UClass* Z_Construct_UClass_Afuse_NoRegister()
	{
		return Afuse::StaticClass();
	}
	UClass* Z_Construct_UClass_Afuse()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_APickup,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "fuse.h" },
				{ "ModuleRelativePath", "fuse.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<Afuse>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&Afuse::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Afuse, 2910823254);
	static FCompiledInDefer Z_CompiledInDefer_UClass_Afuse(Z_Construct_UClass_Afuse, &Afuse::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("Afuse"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Afuse);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
