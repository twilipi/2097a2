// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "switchBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeswitchBase() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AswitchBase_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AswitchBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_changeSwitchStatus();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_getID();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_getIsEnabled();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_getIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_getIsPointedTo();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_getSwitchName();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_glowSwitch();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_setIsEnabled();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_setIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AswitchBase_setIsPointedTo();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AswitchBase_changeSwitchStatus = FName(TEXT("changeSwitchStatus"));
	void AswitchBase::changeSwitchStatus()
	{
		ProcessEvent(FindFunctionChecked(NAME_AswitchBase_changeSwitchStatus),NULL);
	}
	static FName NAME_AswitchBase_glowSwitch = FName(TEXT("glowSwitch"));
	void AswitchBase::glowSwitch()
	{
		ProcessEvent(FindFunctionChecked(NAME_AswitchBase_glowSwitch),NULL);
	}
	void AswitchBase::StaticRegisterNativesAswitchBase()
	{
		UClass* Class = AswitchBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "getID", &AswitchBase::execgetID },
			{ "getIsEnabled", &AswitchBase::execgetIsEnabled },
			{ "getIsGlow", &AswitchBase::execgetIsGlow },
			{ "getIsPointedTo", &AswitchBase::execgetIsPointedTo },
			{ "getSwitchName", &AswitchBase::execgetSwitchName },
			{ "setIsEnabled", &AswitchBase::execsetIsEnabled },
			{ "setIsGlow", &AswitchBase::execsetIsGlow },
			{ "setIsPointedTo", &AswitchBase::execsetIsPointedTo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AswitchBase_changeSwitchStatus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch action" },
				{ "ModuleRelativePath", "switchBase.h" },
				{ "ToolTip", "blueprint overloaded functions\nopen the door" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "changeSwitchStatus", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_getID()
	{
		struct switchBase_eventgetID_Parms
		{
			int32 ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(switchBase_eventgetID_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "getID", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventgetID_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_getIsEnabled()
	{
		struct switchBase_eventgetIsEnabled_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((switchBase_eventgetIsEnabled_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(switchBase_eventgetIsEnabled_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "getIsEnabled", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventgetIsEnabled_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_getIsGlow()
	{
		struct switchBase_eventgetIsGlow_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((switchBase_eventgetIsGlow_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(switchBase_eventgetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "getIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventgetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_getIsPointedTo()
	{
		struct switchBase_eventgetIsPointedTo_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((switchBase_eventgetIsPointedTo_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(switchBase_eventgetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
				{ "ToolTip", "mutators and accessors" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "getIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventgetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_getSwitchName()
	{
		struct switchBase_eventgetSwitchName_Parms
		{
			FString ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(switchBase_eventgetSwitchName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "getSwitchName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventgetSwitchName_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_glowSwitch()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch action" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "glowSwitch", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_setIsEnabled()
	{
		struct switchBase_eventsetIsEnabled_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((switchBase_eventsetIsEnabled_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(switchBase_eventsetIsEnabled_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "setIsEnabled", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventsetIsEnabled_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_setIsGlow()
	{
		struct switchBase_eventsetIsGlow_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((switchBase_eventsetIsGlow_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(switchBase_eventsetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "setIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventsetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AswitchBase_setIsPointedTo()
	{
		struct switchBase_eventsetIsPointedTo_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((switchBase_eventsetIsPointedTo_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(switchBase_eventsetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AswitchBase, "setIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(switchBase_eventsetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AswitchBase_NoRegister()
	{
		return AswitchBase::StaticClass();
	}
	UClass* Z_Construct_UClass_AswitchBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AswitchBase_changeSwitchStatus, "changeSwitchStatus" }, // 2633852369
				{ &Z_Construct_UFunction_AswitchBase_getID, "getID" }, // 2724361395
				{ &Z_Construct_UFunction_AswitchBase_getIsEnabled, "getIsEnabled" }, // 2378849443
				{ &Z_Construct_UFunction_AswitchBase_getIsGlow, "getIsGlow" }, // 2480027804
				{ &Z_Construct_UFunction_AswitchBase_getIsPointedTo, "getIsPointedTo" }, // 2392634660
				{ &Z_Construct_UFunction_AswitchBase_getSwitchName, "getSwitchName" }, // 2260714035
				{ &Z_Construct_UFunction_AswitchBase_glowSwitch, "glowSwitch" }, // 597259768
				{ &Z_Construct_UFunction_AswitchBase_setIsEnabled, "setIsEnabled" }, // 3509603255
				{ &Z_Construct_UFunction_AswitchBase_setIsGlow, "setIsGlow" }, // 2230536943
				{ &Z_Construct_UFunction_AswitchBase_setIsPointedTo, "setIsPointedTo" }, // 662660559
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "switchBase.h" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_switchID_MetaData[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_switchID = { UE4CodeGen_Private::EPropertyClass::Int, "switchID", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, STRUCT_OFFSET(AswitchBase, switchID), METADATA_PARAMS(NewProp_switchID_MetaData, ARRAY_COUNT(NewProp_switchID_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_switchName_MetaData[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_switchName = { UE4CodeGen_Private::EPropertyClass::Str, "switchName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, STRUCT_OFFSET(AswitchBase, switchName), METADATA_PARAMS(NewProp_switchName_MetaData, ARRAY_COUNT(NewProp_switchName_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isGlow_MetaData[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			auto NewProp_isGlow_SetBit = [](void* Obj){ ((AswitchBase*)Obj)->isGlow = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isGlow = { UE4CodeGen_Private::EPropertyClass::Bool, "isGlow", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AswitchBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isGlow_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isGlow_MetaData, ARRAY_COUNT(NewProp_isGlow_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isEnabled_MetaData[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
			};
#endif
			auto NewProp_isEnabled_SetBit = [](void* Obj){ ((AswitchBase*)Obj)->isEnabled = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isEnabled = { UE4CodeGen_Private::EPropertyClass::Bool, "isEnabled", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AswitchBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isEnabled_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isEnabled_MetaData, ARRAY_COUNT(NewProp_isEnabled_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isPointedTo_MetaData[] = {
				{ "Category", "switch control" },
				{ "ModuleRelativePath", "switchBase.h" },
				{ "ToolTip", "switch conditions" },
			};
#endif
			auto NewProp_isPointedTo_SetBit = [](void* Obj){ ((AswitchBase*)Obj)->isPointedTo = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isPointedTo = { UE4CodeGen_Private::EPropertyClass::Bool, "isPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AswitchBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isPointedTo_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isPointedTo_MetaData, ARRAY_COUNT(NewProp_isPointedTo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_switchMesh_MetaData[] = {
				{ "Category", "switchBase" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "switchBase.h" },
				{ "ToolTip", "meshes" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_switchMesh = { UE4CodeGen_Private::EPropertyClass::Object, "switchMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AswitchBase, switchMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_switchMesh_MetaData, ARRAY_COUNT(NewProp_switchMesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_switchID,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_switchName,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isGlow,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isEnabled,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isPointedTo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_switchMesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AswitchBase>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AswitchBase::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AswitchBase, 1595305205);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AswitchBase(Z_Construct_UClass_AswitchBase, &AswitchBase::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("AswitchBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AswitchBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
