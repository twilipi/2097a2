// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "fuseBayBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodefuseBayBase() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AfuseBayBase_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AfuseBayBase();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_getFuseBayName();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_getIsFuseGet();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_getIsFuseSet();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_getIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_getIsPointedTo();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_glowBay();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_glowFuse();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_setFuse();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_setIsFuseGet();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_setIsFuseSet();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_setIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_AfuseBayBase_setIsPointedTo();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_AfuseBayBase_glowBay = FName(TEXT("glowBay"));
	void AfuseBayBase::glowBay()
	{
		ProcessEvent(FindFunctionChecked(NAME_AfuseBayBase_glowBay),NULL);
	}
	static FName NAME_AfuseBayBase_glowFuse = FName(TEXT("glowFuse"));
	void AfuseBayBase::glowFuse()
	{
		ProcessEvent(FindFunctionChecked(NAME_AfuseBayBase_glowFuse),NULL);
	}
	void AfuseBayBase::StaticRegisterNativesAfuseBayBase()
	{
		UClass* Class = AfuseBayBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "getFuseBayName", &AfuseBayBase::execgetFuseBayName },
			{ "getIsFuseGet", &AfuseBayBase::execgetIsFuseGet },
			{ "getIsFuseSet", &AfuseBayBase::execgetIsFuseSet },
			{ "getIsGlow", &AfuseBayBase::execgetIsGlow },
			{ "getIsPointedTo", &AfuseBayBase::execgetIsPointedTo },
			{ "setFuse", &AfuseBayBase::execsetFuse },
			{ "setIsFuseGet", &AfuseBayBase::execsetIsFuseGet },
			{ "setIsFuseSet", &AfuseBayBase::execsetIsFuseSet },
			{ "setIsGlow", &AfuseBayBase::execsetIsGlow },
			{ "setIsPointedTo", &AfuseBayBase::execsetIsPointedTo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_getFuseBayName()
	{
		struct fuseBayBase_eventgetFuseBayName_Parms
		{
			FString ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(fuseBayBase_eventgetFuseBayName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
				{ "ToolTip", "mutators and accessors" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "getFuseBayName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventgetFuseBayName_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_getIsFuseGet()
	{
		struct fuseBayBase_eventgetIsFuseGet_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((fuseBayBase_eventgetIsFuseGet_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventgetIsFuseGet_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "getIsFuseGet", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventgetIsFuseGet_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_getIsFuseSet()
	{
		struct fuseBayBase_eventgetIsFuseSet_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((fuseBayBase_eventgetIsFuseSet_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventgetIsFuseSet_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "getIsFuseSet", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventgetIsFuseSet_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_getIsGlow()
	{
		struct fuseBayBase_eventgetIsGlow_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((fuseBayBase_eventgetIsGlow_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventgetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "getIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventgetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_getIsPointedTo()
	{
		struct fuseBayBase_eventgetIsPointedTo_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((fuseBayBase_eventgetIsPointedTo_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventgetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "getIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventgetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_glowBay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay action" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "glowBay", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_glowFuse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay action" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
				{ "ToolTip", "blueprint overloaded functions" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "glowFuse", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_setFuse()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay action" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
				{ "ToolTip", "set up the fuse if fuse is get" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "setFuse", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_setIsFuseGet()
	{
		struct fuseBayBase_eventsetIsFuseGet_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((fuseBayBase_eventsetIsFuseGet_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventsetIsFuseGet_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "setIsFuseGet", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventsetIsFuseGet_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_setIsFuseSet()
	{
		struct fuseBayBase_eventsetIsFuseSet_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((fuseBayBase_eventsetIsFuseSet_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventsetIsFuseSet_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "setIsFuseSet", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventsetIsFuseSet_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_setIsGlow()
	{
		struct fuseBayBase_eventsetIsGlow_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((fuseBayBase_eventsetIsGlow_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventsetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "setIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventsetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AfuseBayBase_setIsPointedTo()
	{
		struct fuseBayBase_eventsetIsPointedTo_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((fuseBayBase_eventsetIsPointedTo_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(fuseBayBase_eventsetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AfuseBayBase, "setIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(fuseBayBase_eventsetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AfuseBayBase_NoRegister()
	{
		return AfuseBayBase::StaticClass();
	}
	UClass* Z_Construct_UClass_AfuseBayBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AfuseBayBase_getFuseBayName, "getFuseBayName" }, // 748094343
				{ &Z_Construct_UFunction_AfuseBayBase_getIsFuseGet, "getIsFuseGet" }, // 97067989
				{ &Z_Construct_UFunction_AfuseBayBase_getIsFuseSet, "getIsFuseSet" }, // 1244888395
				{ &Z_Construct_UFunction_AfuseBayBase_getIsGlow, "getIsGlow" }, // 2834400176
				{ &Z_Construct_UFunction_AfuseBayBase_getIsPointedTo, "getIsPointedTo" }, // 3693607668
				{ &Z_Construct_UFunction_AfuseBayBase_glowBay, "glowBay" }, // 1937817803
				{ &Z_Construct_UFunction_AfuseBayBase_glowFuse, "glowFuse" }, // 3646285986
				{ &Z_Construct_UFunction_AfuseBayBase_setFuse, "setFuse" }, // 1577895934
				{ &Z_Construct_UFunction_AfuseBayBase_setIsFuseGet, "setIsFuseGet" }, // 2310025633
				{ &Z_Construct_UFunction_AfuseBayBase_setIsFuseSet, "setIsFuseSet" }, // 3455859642
				{ &Z_Construct_UFunction_AfuseBayBase_setIsGlow, "setIsGlow" }, // 358588233
				{ &Z_Construct_UFunction_AfuseBayBase_setIsPointedTo, "setIsPointedTo" }, // 2511615656
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "fuseBayBase.h" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fuseBayName_MetaData[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_fuseBayName = { UE4CodeGen_Private::EPropertyClass::Str, "fuseBayName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, STRUCT_OFFSET(AfuseBayBase, fuseBayName), METADATA_PARAMS(NewProp_fuseBayName_MetaData, ARRAY_COUNT(NewProp_fuseBayName_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isGlow_MetaData[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			auto NewProp_isGlow_SetBit = [](void* Obj){ ((AfuseBayBase*)Obj)->isGlow = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isGlow = { UE4CodeGen_Private::EPropertyClass::Bool, "isGlow", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AfuseBayBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isGlow_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isGlow_MetaData, ARRAY_COUNT(NewProp_isGlow_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isFuseSet_MetaData[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			auto NewProp_isFuseSet_SetBit = [](void* Obj){ ((AfuseBayBase*)Obj)->isFuseSet = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isFuseSet = { UE4CodeGen_Private::EPropertyClass::Bool, "isFuseSet", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AfuseBayBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isFuseSet_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isFuseSet_MetaData, ARRAY_COUNT(NewProp_isFuseSet_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isFuseGet_MetaData[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			auto NewProp_isFuseGet_SetBit = [](void* Obj){ ((AfuseBayBase*)Obj)->isFuseGet = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isFuseGet = { UE4CodeGen_Private::EPropertyClass::Bool, "isFuseGet", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AfuseBayBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isFuseGet_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isFuseGet_MetaData, ARRAY_COUNT(NewProp_isFuseGet_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isPointedTo_MetaData[] = {
				{ "Category", "fuse bay control" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			auto NewProp_isPointedTo_SetBit = [](void* Obj){ ((AfuseBayBase*)Obj)->isPointedTo = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isPointedTo = { UE4CodeGen_Private::EPropertyClass::Bool, "isPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AfuseBayBase), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isPointedTo_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isPointedTo_MetaData, ARRAY_COUNT(NewProp_isPointedTo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fuseCore_MetaData[] = {
				{ "Category", "fuseBayBase" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_fuseCore = { UE4CodeGen_Private::EPropertyClass::Object, "fuseCore", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AfuseBayBase, fuseCore), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_fuseCore_MetaData, ARRAY_COUNT(NewProp_fuseCore_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fuseBay_MetaData[] = {
				{ "Category", "fuseBayBase" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "fuseBayBase.h" },
				{ "ToolTip", "meshes" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_fuseBay = { UE4CodeGen_Private::EPropertyClass::Object, "fuseBay", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(AfuseBayBase, fuseBay), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_fuseBay_MetaData, ARRAY_COUNT(NewProp_fuseBay_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_fuseBayName,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isGlow,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isFuseSet,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isFuseGet,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isPointedTo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_fuseCore,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_fuseBay,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AfuseBayBase>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AfuseBayBase::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AfuseBayBase, 3583333056);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AfuseBayBase(Z_Construct_UClass_AfuseBayBase, &AfuseBayBase::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("AfuseBayBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AfuseBayBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
