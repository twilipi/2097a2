// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Pickup.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePickup() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_APickup_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_APickup();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_getIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_getIsPointedTo();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_getPickupName();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_pickupAction();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_pickupGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_setIsGlow();
	FIT2097_A2TEST_API UFunction* Z_Construct_UFunction_APickup_setIsPointedTo();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	static FName NAME_APickup_pickupAction = FName(TEXT("pickupAction"));
	void APickup::pickupAction()
	{
		ProcessEvent(FindFunctionChecked(NAME_APickup_pickupAction),NULL);
	}
	static FName NAME_APickup_pickupGlow = FName(TEXT("pickupGlow"));
	void APickup::pickupGlow()
	{
		ProcessEvent(FindFunctionChecked(NAME_APickup_pickupGlow),NULL);
	}
	void APickup::StaticRegisterNativesAPickup()
	{
		UClass* Class = APickup::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "getIsGlow", &APickup::execgetIsGlow },
			{ "getIsPointedTo", &APickup::execgetIsPointedTo },
			{ "getPickupName", &APickup::execgetPickupName },
			{ "setIsGlow", &APickup::execsetIsGlow },
			{ "setIsPointedTo", &APickup::execsetIsPointedTo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_APickup_getIsGlow()
	{
		struct Pickup_eventgetIsGlow_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((Pickup_eventgetIsGlow_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Pickup_eventgetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "getIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Pickup_eventgetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APickup_getIsPointedTo()
	{
		struct Pickup_eventgetIsPointedTo_Parms
		{
			bool ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_ReturnValue_SetBit = [](void* Obj){ ((Pickup_eventgetIsPointedTo_Parms*)Obj)->ReturnValue = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Pickup_eventgetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_ReturnValue_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "getIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Pickup_eventgetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APickup_getPickupName()
	{
		struct Pickup_eventgetPickupName_Parms
		{
			FString ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(Pickup_eventgetPickupName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
				{ "ToolTip", "mutators and accessors" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "getPickupName", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Pickup_eventgetPickupName_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APickup_pickupAction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch action" },
				{ "ModuleRelativePath", "Pickup.h" },
				{ "ToolTip", "blueprint overloaded functions\npickup the object and then destory the object" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "pickupAction", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APickup_pickupGlow()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "switch action" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "pickupGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x08020800, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APickup_setIsGlow()
	{
		struct Pickup_eventsetIsGlow_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((Pickup_eventsetIsGlow_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Pickup_eventsetIsGlow_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "setIsGlow", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Pickup_eventsetIsGlow_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APickup_setIsPointedTo()
	{
		struct Pickup_eventsetIsPointedTo_Parms
		{
			bool inBool;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			auto NewProp_inBool_SetBit = [](void* Obj){ ((Pickup_eventsetIsPointedTo_Parms*)Obj)->inBool = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_inBool = { UE4CodeGen_Private::EPropertyClass::Bool, "inBool", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(Pickup_eventsetIsPointedTo_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_inBool_SetBit)>::SetBit, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_inBool,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APickup, "setIsPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(Pickup_eventsetIsPointedTo_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APickup_NoRegister()
	{
		return APickup::StaticClass();
	}
	UClass* Z_Construct_UClass_APickup()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_APickup_getIsGlow, "getIsGlow" }, // 1264611721
				{ &Z_Construct_UFunction_APickup_getIsPointedTo, "getIsPointedTo" }, // 2590798437
				{ &Z_Construct_UFunction_APickup_getPickupName, "getPickupName" }, // 3998848346
				{ &Z_Construct_UFunction_APickup_pickupAction, "pickupAction" }, // 2439645308
				{ &Z_Construct_UFunction_APickup_pickupGlow, "pickupGlow" }, // 3954499190
				{ &Z_Construct_UFunction_APickup_setIsGlow, "setIsGlow" }, // 2344386892
				{ &Z_Construct_UFunction_APickup_setIsPointedTo, "setIsPointedTo" }, // 412615754
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Pickup.h" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pickupName_MetaData[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_pickupName = { UE4CodeGen_Private::EPropertyClass::Str, "pickupName", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000001, 1, nullptr, STRUCT_OFFSET(APickup, pickupName), METADATA_PARAMS(NewProp_pickupName_MetaData, ARRAY_COUNT(NewProp_pickupName_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isGlow_MetaData[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			auto NewProp_isGlow_SetBit = [](void* Obj){ ((APickup*)Obj)->isGlow = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isGlow = { UE4CodeGen_Private::EPropertyClass::Bool, "isGlow", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APickup), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isGlow_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isGlow_MetaData, ARRAY_COUNT(NewProp_isGlow_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isPointedTo_MetaData[] = {
				{ "Category", "pickup control" },
				{ "ModuleRelativePath", "Pickup.h" },
			};
#endif
			auto NewProp_isPointedTo_SetBit = [](void* Obj){ ((APickup*)Obj)->isPointedTo = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isPointedTo = { UE4CodeGen_Private::EPropertyClass::Bool, "isPointedTo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0020080000000004, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(APickup), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_isPointedTo_SetBit)>::SetBit, METADATA_PARAMS(NewProp_isPointedTo_MetaData, ARRAY_COUNT(NewProp_isPointedTo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pickupMesh_MetaData[] = {
				{ "Category", "Pickup" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "Pickup.h" },
				{ "ToolTip", "meshes" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_pickupMesh = { UE4CodeGen_Private::EPropertyClass::Object, "pickupMesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x001000000008000d, 1, nullptr, STRUCT_OFFSET(APickup, pickupMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(NewProp_pickupMesh_MetaData, ARRAY_COUNT(NewProp_pickupMesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_pickupName,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isGlow,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_isPointedTo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_pickupMesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APickup>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APickup::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APickup, 2588554241);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APickup(Z_Construct_UClass_APickup, &APickup::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("APickup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APickup);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
