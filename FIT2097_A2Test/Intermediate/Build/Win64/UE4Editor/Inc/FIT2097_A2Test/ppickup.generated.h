// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_ppickup_generated_h
#error "ppickup.generated.h already included, missing '#pragma once' in ppickup.h"
#endif
#define FIT2097_A2TEST_ppickup_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_RPC_WRAPPERS
#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAppickup(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_Appickup(); \
public: \
	DECLARE_CLASS(Appickup, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(Appickup) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAppickup(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_Appickup(); \
public: \
	DECLARE_CLASS(Appickup, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(Appickup) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Appickup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Appickup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Appickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Appickup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Appickup(Appickup&&); \
	NO_API Appickup(const Appickup&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Appickup(Appickup&&); \
	NO_API Appickup(const Appickup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Appickup); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Appickup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Appickup)


#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_PRIVATE_PROPERTY_OFFSET
#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_9_PROLOG
#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_ppickup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
