// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_testDoor_generated_h
#error "testDoor.generated.h already included, missing '#pragma once' in testDoor.h"
#endif
#define FIT2097_A2TEST_testDoor_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetPickupName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetPickupName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsLocked) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsLocked(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsPointedTo) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsPointedTo(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsLocked) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsLocked(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsPointedTo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsPointedTo(); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetPickupName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetPickupName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsLocked) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsLocked(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetIsPointedTo) \
	{ \
		P_GET_UBOOL(Z_Param_inBool); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setIsPointedTo(Z_Param_inBool); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsLocked) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsLocked(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetIsPointedTo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->getIsPointedTo(); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_EVENT_PARMS
#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_CALLBACK_WRAPPERS
#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAtestDoor(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AtestDoor(); \
public: \
	DECLARE_CLASS(AtestDoor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AtestDoor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAtestDoor(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AtestDoor(); \
public: \
	DECLARE_CLASS(AtestDoor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AtestDoor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AtestDoor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AtestDoor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AtestDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AtestDoor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AtestDoor(AtestDoor&&); \
	NO_API AtestDoor(const AtestDoor&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AtestDoor(AtestDoor&&); \
	NO_API AtestDoor(const AtestDoor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AtestDoor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AtestDoor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AtestDoor)


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__isPointedTo() { return STRUCT_OFFSET(AtestDoor, isPointedTo); } \
	FORCEINLINE static uint32 __PPO__isLocked() { return STRUCT_OFFSET(AtestDoor, isLocked); }


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_9_PROLOG \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_EVENT_PARMS


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_CALLBACK_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_CALLBACK_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_testDoor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
