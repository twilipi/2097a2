// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_FIT2097_A2TestCharacter_generated_h
#error "FIT2097_A2TestCharacter.generated.h already included, missing '#pragma once' in FIT2097_A2TestCharacter.h"
#endif
#define FIT2097_A2TEST_FIT2097_A2TestCharacter_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execprocessEEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->processEEvent(); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execprocessEEvent) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->processEEvent(); \
		P_NATIVE_END; \
	}


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFIT2097_A2TestCharacter(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AFIT2097_A2TestCharacter(); \
public: \
	DECLARE_CLASS(AFIT2097_A2TestCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AFIT2097_A2TestCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAFIT2097_A2TestCharacter(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AFIT2097_A2TestCharacter(); \
public: \
	DECLARE_CLASS(AFIT2097_A2TestCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AFIT2097_A2TestCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFIT2097_A2TestCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT2097_A2TestCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT2097_A2TestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097_A2TestCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT2097_A2TestCharacter(AFIT2097_A2TestCharacter&&); \
	NO_API AFIT2097_A2TestCharacter(const AFIT2097_A2TestCharacter&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT2097_A2TestCharacter(AFIT2097_A2TestCharacter&&); \
	NO_API AFIT2097_A2TestCharacter(const AFIT2097_A2TestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT2097_A2TestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097_A2TestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFIT2097_A2TestCharacter)


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AFIT2097_A2TestCharacter, L_MotionController); }


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_14_PROLOG
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
