// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "FIT2097_A2TestHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFIT2097_A2TestHUD() {}
// Cross Module References
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AFIT2097_A2TestHUD_NoRegister();
	FIT2097_A2TEST_API UClass* Z_Construct_UClass_AFIT2097_A2TestHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_FIT2097_A2Test();
// End Cross Module References
	void AFIT2097_A2TestHUD::StaticRegisterNativesAFIT2097_A2TestHUD()
	{
	}
	UClass* Z_Construct_UClass_AFIT2097_A2TestHUD_NoRegister()
	{
		return AFIT2097_A2TestHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_AFIT2097_A2TestHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_FIT2097_A2Test,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "FIT2097_A2TestHUD.h" },
				{ "ModuleRelativePath", "FIT2097_A2TestHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AFIT2097_A2TestHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AFIT2097_A2TestHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFIT2097_A2TestHUD, 1258799835);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFIT2097_A2TestHUD(Z_Construct_UClass_AFIT2097_A2TestHUD, &AFIT2097_A2TestHUD::StaticClass, TEXT("/Script/FIT2097_A2Test"), TEXT("AFIT2097_A2TestHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFIT2097_A2TestHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
