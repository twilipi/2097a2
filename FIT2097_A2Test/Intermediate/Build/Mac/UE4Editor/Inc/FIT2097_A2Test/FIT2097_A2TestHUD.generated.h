// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIT2097_A2TEST_FIT2097_A2TestHUD_generated_h
#error "FIT2097_A2TestHUD.generated.h already included, missing '#pragma once' in FIT2097_A2TestHUD.h"
#endif
#define FIT2097_A2TEST_FIT2097_A2TestHUD_generated_h

#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_RPC_WRAPPERS
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFIT2097_A2TestHUD(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AFIT2097_A2TestHUD(); \
public: \
	DECLARE_CLASS(AFIT2097_A2TestHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AFIT2097_A2TestHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFIT2097_A2TestHUD(); \
	friend FIT2097_A2TEST_API class UClass* Z_Construct_UClass_AFIT2097_A2TestHUD(); \
public: \
	DECLARE_CLASS(AFIT2097_A2TestHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/FIT2097_A2Test"), NO_API) \
	DECLARE_SERIALIZER(AFIT2097_A2TestHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFIT2097_A2TestHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFIT2097_A2TestHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT2097_A2TestHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097_A2TestHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT2097_A2TestHUD(AFIT2097_A2TestHUD&&); \
	NO_API AFIT2097_A2TestHUD(const AFIT2097_A2TestHUD&); \
public:


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFIT2097_A2TestHUD(AFIT2097_A2TestHUD&&); \
	NO_API AFIT2097_A2TestHUD(const AFIT2097_A2TestHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFIT2097_A2TestHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFIT2097_A2TestHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFIT2097_A2TestHUD)


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_9_PROLOG
#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_RPC_WRAPPERS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_INCLASS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_INCLASS_NO_PURE_DECLS \
	FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FIT2097_A2Test_Source_FIT2097_A2Test_FIT2097_A2TestHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
